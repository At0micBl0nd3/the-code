# Getting Started

## Schedule
The Cyber Team practices Monday-Friday from 3:45-5:15. We compete in competitions, including Capture the Flag (CTF), Attack/Defense, and Cyber Policy events, on most weekends. Please join our Office 365 group with [this](https://outlook.office365.com/owa/df_dfcs-cyberteam@afacademy.af.edu/groupsubscription.ashx?action=join&source=MSExchange/LokiServer&guid=fff78fa4-7900-44b4-9afd-bf7b05587853) link. This will share our calender which is updated with practice times and competition information.